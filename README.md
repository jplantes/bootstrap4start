#Instrucciones de uso

## 1.- Clonar repositorio

> git clone https://jplantes@bitbucket.org/jplantes/bootstrap4start.git

##2.- Renombra la plantilla

> cp template.html index.html

##3.- Comienza a maquetar

#Componente en el paquete

 * Bootstrap4
 * Font Awesome 4.7

#Estructura de archivos

```
|-- assets
|   |-- css
|   |   |-- bootstrap.min.css
|   |   |-- estilo.css
|   |   `-- font-awesome.min.css
|   |-- fonts
|   |   |-- FontAwesome.otf
|   |   |-- fontawesome-webfont.eot
|   |   |-- fontawesome-webfont.svg
|   |   |-- fontawesome-webfont.ttf
|   |   |-- fontawesome-webfont.woff
|   |   `-- fontawesome-webfont.woff2
|   |-- img
|   `-- js
|       |-- bootstrap.min.js
|       |-- jquery-3.3.1.slim.min.js
|       |-- popper.min.js
|       `-- script.js
`-- template.html
```

> jplantes